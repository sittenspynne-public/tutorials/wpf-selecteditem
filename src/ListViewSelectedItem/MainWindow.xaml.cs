﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ListViewSelectedItem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var people = new[]
            {
                new Person() { Age = 25, Name ="Person 1 Name" },
                new Person() { Age = 30, Name = "Person 2 Name" },
                new Person() { Age = 10, Name = "Person 3 Name" }
            };

            var vm = new MainWindowViewModel();
            vm.People = new System.Collections.ObjectModel.ObservableCollection<Person>(people);
            this.DataContext = vm;
        }
    }
}
